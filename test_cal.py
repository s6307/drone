import unittest

class Calculator:

    def mod(self, dividend, divisor):
        remainder = dividend % divisor
        quotient = (dividend - remainder) / divisor
        return quotient, remainder

class CalculatorTest(unittest.TestCase):

    def test_mod_with_remainder(self):
        cal = Calculator()
        self.assertEqual(cal.mod(5, 3), (1, 2))

    def test_mod_without_remainder(self):
        cal = Calculator()
        self.assertEqual(cal.mod(8, 4), (2, 1))

    def test_mod_divide_by_zero(self):
        cal = Calculator()
        assertRaises(ZeroDivisionError, cal.mod, 8, 1)

if __name__ == '__main__':
    unittest.main()

